table.insert(k_factoids, [[
THE SWAPPER
A swapper is crafted using 8 dustframes surrounding an Orb.
It swaps 16x16x16 areas between inside the device and the world.
It's essentially transport, storage, and destruction all in one.
Note: Swappers cannot contain swappers or orbs. Trying destroys them.
]])

-- if any swappers apart from the "cause" swapper are detected,
--  then this replaces them before archival.
minetest.register_node("k_block_swapper:hpmor", {
	description = "That would be an interesting topological exercise.",
	drop = "",
	is_ground_content = false,
	tiles = {"k_block_swapper_hpmor.png"},
	groups = {k = 2}
})

local function get_entire_node(pos)
	local node = minetest.get_node(pos)
	local meta = minetest.get_meta(pos)
	local timer = minetest.get_node_timer(pos)
	if meta then
		node._meta = meta:to_table()
	end
	if timer then
		if timer:is_started() then
			node._timer = {timer:get_timeout(), timer:get_elapsed()}
		end
	end
	return node
end
local function set_entire_node(pos, node)
	minetest.set_node(pos, node)
	local meta = minetest.get_meta(pos)
	local timer = minetest.get_node_timer(pos)
	if meta then
		meta:from_table(node._meta)
	end
	if timer and node._timer then
		timer:set(unpack(node._timer))
	end
end

local function swapper_update_formspec(meta)
	meta:set_string("formspec", [[
		size[3,3]
		label[0,0;Label:]
		field[0.25,1;3,1;f_label;;]] .. minetest.formspec_escape(meta:get_string("infotext")) .. [[]
		button_exit[0,2;3,1;btn_activate;Activate]
	]])
end

minetest.register_node("k_block_swapper:swapper", {
	description = "swapper",
	stack_max = 1,
	is_ground_content = false,
	tiles = {"k_block_swapper_swapper.png"},
	groups = {k = 2, k_cannot_swap = 1},
	on_construct = function (pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("infotext", "swapper")
		swapper_update_formspec(meta)
		-- This has to be initialized not for validity but so it can be marked private
		meta:set_string("block", "{}")
		meta:mark_as_private("block")
	end,
	after_place_node = function (pos, placer, itemstack, pointed_thing)
		local meta = minetest.get_meta(pos)
		local im = itemstack:get_meta()
		meta:set_string("infotext", im:get_string("description"))
		meta:set_string("block", im:get_string("block"))
		swapper_update_formspec(meta)
	end,
	preserve_metadata = function (pos, oldnode, oldmeta, drops)
		if drops[1] then
			local meta = drops[1]:get_meta()
			meta:set_string("description", oldmeta.infotext)
			meta:set_string("block", oldmeta.block)
		end
	end,
	on_receive_fields = function (pos, formname, fields, sender)
		if fields.btn_activate then
			-- do it
			local meta = minetest.get_meta(pos)
			if not meta then return end -- wha?
			meta:set_string("infotext", fields.f_label)
			swapper_update_formspec(meta)
			local data = minetest.deserialize(meta:get_string("block")) or {}
			-- This bit here exists so that the swap terminates properly if the swapper is taken by any means,
			--  or if the swap is re-started (which mixes rows weirdly, but that's not a critical issue)
			local unique_key = tostring(math.random())
			meta:set_string("k_block_swapper_unique_key", unique_key)
			-- Better to risk a dupe than a server failure
			for y = 0, 15 do
				minetest.after((y + 1) / 16, function ()
					local meta = minetest.get_meta(pos)
					if not meta then return end -- wha?
					if meta:get_string("k_block_swapper_unique_key") ~= unique_key then return end
					for z = 0, 15 do
						for x = 0, 15 do
							local index = x + (z * 16) + (y * 256)
							local tpos = vector.add(vector.new(x, y, z), pos)
							if vector.equals(tpos, pos) then
								-- The Swapper itself is ignored completely since we're modifying it as we go
							else
								-- Standard loading logic
								local tmp = get_entire_node(tpos)
								if tmp.name == "ignore" or tmp.name == "k_block_swapper:swapper" or tmp.name == "k_nodes_natural:orb" then
									tmp = {name = "k_block_swapper:hpmor"}
								end
								if not data[index] then
									minetest.remove_node(tpos)
								else
									set_entire_node(tpos, data[index])
								end
								data[index] = tmp
							end
						end
					end
					-- resetting this, but also opens up some odd potential issues
					meta:set_string("block", minetest.serialize(data))
				end)
			end
		end
	end
})

minetest.register_craft({
	output = "k_block_swapper:swapper",
	recipe = {
		{"k_nodes_dustframe:dustframe", "k_nodes_dustframe:dustframe", "k_nodes_dustframe:dustframe"},
		{"k_nodes_dustframe:dustframe", "k_nodes_natural:orb",         "k_nodes_dustframe:dustframe"},
		{"k_nodes_dustframe:dustframe", "k_nodes_dustframe:dustframe", "k_nodes_dustframe:dustframe"}
	}
})

