minetest.register_node("k_nodes_dustframe:dustframe", {
	description = "dustframe",
	is_ground_content = false,
	drawtype = "glasslike_framed",
	tiles = {"k_nodes_dustframe_1.png", "k_nodes_dustframe_2.png"},
	groups = {k = 1},
	paramtype = "light",
	sunlight_propagates = true
})
minetest.register_craft({
	output = "k_nodes_dustframe:dustframe",
	recipe = {
		{"k_nodes_natural:light", "k_nodes_natural:light", "k_nodes_natural:light"},
		{"k_nodes_natural:light", "",                      "k_nodes_natural:light"},
		{"k_nodes_natural:light", "k_nodes_natural:light", "k_nodes_natural:light"}
	}
})

table.insert(k_factoids, [[
DUSTFRAMES
Putting dust into a square 'frame' shape in the crafting area creates a dustframe.
A dustframe is transparent, but prevents moving through by accident.
It also has other uses in crafting.
]])

