local function create_compressed_recipe(a, b)
	minetest.register_craft({
		output = b,
		recipe = {
			{a, a},
			{a, a}
		}
	})
	minetest.register_craft({
		output = a .. " 4",
		type = "shapeless",
		recipe = {
			b
		}
	})
end
local function create_compressed_node(id)
	minetest.register_node("k_nodes_compression:x" .. id, {
		description = "compressed substrate " .. id,
		is_ground_content = false,
		tiles = {"k_nodes_compression_x" .. id .. ".png"},
		groups = {k = 1}
	})
end

create_compressed_node(1)
create_compressed_node(2)
create_compressed_node(3)
create_compressed_node(4)
create_compressed_node(5)

create_compressed_recipe("k_nodes_natural:light", "k_nodes_natural:heavy")
create_compressed_recipe("k_nodes_natural:heavy", "k_nodes_compression:x1") -- 4
create_compressed_recipe("k_nodes_compression:x1", "k_nodes_compression:x2") -- 16
create_compressed_recipe("k_nodes_compression:x2", "k_nodes_compression:x3") -- 64
create_compressed_recipe("k_nodes_compression:x3", "k_nodes_compression:x4") -- 256
create_compressed_recipe("k_nodes_compression:x4", "k_nodes_compression:x5") -- 1024
-- if you actually get this far, you've mined at least 4096 substrate, so here have your Orb
minetest.register_craft({
	output = "k_nodes_natural:orb",
	recipe = {
		{"k_nodes_compression:x5", "k_nodes_compression:x5"},
		{"k_nodes_compression:x5", "k_nodes_compression:x5"}
	}
})

table.insert(k_factoids, [[
COMPRESSING DUST INTO SUBSTRATE AND INTO ORBS
4 dust can be compressed into 1 substrate.
4 substrate can be compressed into 1 compressed substrate.
If you take the compression far enough, you can create an Orb.
This is a useful, if slow, alternative method of creating an Orb if you can't get one otherwise.
]])

