minetest.register_node("k_nodes_gel:gel", {
	description = "gel",
	is_ground_content = false,
	tiles = {"k_nodes_gel_gel.png"},
	groups = {k = 1, bouncy = 105, slippery = 10, fall_damage_add_percent = -9999}
})

minetest.register_craft({
	output = "k_nodes_gel:gel 99",
	type = "shapeless",
	recipe = {
		"k_nodes_natural:orb"
	}
})

table.insert(k_factoids, [[
GEL
Using an orb, alone, in crafting, creates 99 gel.
The primary use of gel is that gel is bouncy.
This allows it to prevent fall damage, though it also sends you into the air.
But it's also a crafting ingredient for things like polished RGB nodes.
]])

