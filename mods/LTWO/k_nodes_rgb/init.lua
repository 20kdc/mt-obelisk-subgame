for r = 0, 5 do
	for g = 0, 5 do
		for b = 0, 5 do
			local cstring = string.format("#%02x%02x%02x", r * 51, g * 51, b * 51)
			minetest.register_node("k_nodes_rgb:light" .. r .. g .. b, {
				description = "rgb light " .. r .. g .. b,
				is_ground_content = false,
				drawtype = "glasslike_framed",
				tiles = {"k_nodes_rgb_light.png^[multiply:" .. cstring, "k_nodes_rgb_flat.png^[multiply:" .. cstring},
				groups = {k = 1},
				paramtype = "light",
				sunlight_propagates = true,
				light_source = minetest.LIGHT_MAX
			})
			minetest.register_node("k_nodes_rgb:flat" .. r .. g .. b, {
				description = "rgb flat " .. r .. g .. b,
				is_ground_content = false,
				tiles = {"k_nodes_rgb_flat.png^[multiply:" .. cstring},
				groups = {k = 1}
			})
			minetest.register_node("k_nodes_rgb:polish" .. r .. g .. b, {
				drawtype = "mesh",
				mesh = "k_nodes_rgb_polish.obj",
				description = "rgb polish " .. r .. g .. b,
				is_ground_content = false,
				paramtype = "light",
				sunlight_propagates = true,
				tiles = {"k_nodes_rgb_polish.png^[multiply:" .. cstring},
				groups = {k = 1}
			})
			-- light -> flat via dust --
			minetest.register_craft({
				type = "shapeless",
				output = "k_nodes_rgb:flat" .. r .. g .. b .. " 2",
				recipe = {
					"k_nodes_rgb:light" .. r .. g .. b,
					"k_nodes_natural:light"
				}
			})
			-- flat -> polish via gel --
			minetest.register_craft({
				type = "shapeless",
				output = "k_nodes_rgb:polish" .. r .. g .. b .. " 2",
				recipe = {
					"k_nodes_rgb:flat" .. r .. g .. b,
					"k_nodes_gel:gel"
				}
			})
		end
	end
end
-- averaging recipe --
for r1 = 0, 5 do
	for g1 = 0, 5 do
		for b1 = 0, 5 do
			for r2 = 0, 5 do
				for g2 = 0, 5 do
					for b2 = 0, 5 do
						local r3, g3, b3 = math.floor((r1 + r2) / 2), math.floor((g1 + g2) / 2), math.floor((b1 + b2) / 2)
						minetest.register_craft({
							type = "shapeless",
							output = "k_nodes_rgb:light" .. r3 .. g3 .. b3 .. " 2",
							recipe = {
								"k_nodes_rgb:light" .. r1 .. g1 .. b1,
								"k_nodes_rgb:light" .. r2 .. g2 .. b2
							}
						})
						minetest.register_craft({
							type = "shapeless",
							output = "k_nodes_rgb:flat" .. r3 .. g3 .. b3 .. " 2",
							recipe = {
								"k_nodes_rgb:flat" .. r1 .. g1 .. b1,
								"k_nodes_rgb:flat" .. r2 .. g2 .. b2
							}
						})
					end
				end
			end
		end
	end
end
-- base colours
for r = 0, 1 do
	for g = 0, 1 do
		for b = 0, 1 do
			local i = {
				[0] = "k_nodes_natural:heavy",
				[1] = "k_nodes_natural:light"
			}
			minetest.register_craft({
				output = "k_nodes_rgb:light" .. (r * 5) .. (g * 5) .. (b * 5),
				recipe = {
					{"", "k_nodes_natural:glow", ""},
					{"", "k_nodes_dustframe:dustframe", ""},
					{i[r], i[g], i[b]}
				}
			})
		end
	end
end

table.insert(k_factoids, [[
RGB NODES
216 separate colours are available, called 'rgb nodes'.
You create 8 basic lamp colours using the following recipe:
1 lumastrate, under that 1 dustframe, and under that, 3 substrate/dust.
The choice of substrate vs. dust in each of the 3 places determines which base colour you get.
Mixing these mixes the colours, allowing for more complex colour combinations.
You can mix them with additional dust for two non-lamp flat rgb nodes.
]])

table.insert(k_factoids, [[
POLISHED RGB NODES
Combining a flat rgb node with gel creates 2 polished rgb nodes.
These are intended to look extremely fancy, but you can't mix them.
And you can't do much else with them, either...
]])

