--[[
Y Export Selected Objects Only
Coordinat... Right-Handed
Up Axis: Y
Y Export Meshes
Y Export Normals
N Flip Normals
Y Export UV Coordinates
Y Export Materials
N Reference etc.
N Export Vertex Colors
Y Export Skin Weights
N Apply Modifiers
Y Export Armature Bones
Y Export Rest Position
(the rest are N)
--]]

local function gen_default()
	-- player.png
	local palette = {
		-- NOTE: Even if something is overridden later, keep the palette entries
		["#"] = 215,
		H = 21,
		h = 64,
		S = 8,
		s = 14,
		E = 115,
		e = 120,
		l = 12
	}

	local colours = {
		{21, 64}, -- red
		{21, 27}, -- pink
		{115, 120}, -- blue
		{138, 139}, -- purple
		{22, 28} -- yellow
	}

	local function setup(a, b)
		local r = colours[math.random(#colours)]
		palette[a] = r[1]
		palette[b] = r[2]
	end

	setup("E", "e")
	setup("H", "h")

	if math.random() > 0.5 then
		-- Secondary skin colour palette
		palette.S = 15
		palette.s = 22
	end
	local part1 = [[
		hHhHhHhHhHhHhHhH
		hHhHhHhHhHhHhHhH
		hHhHhHhHHhHhHhHh
		hHhHhHhHHhHhHhHh
		HhHhHhHhhHhHhHhH
		HhHhHhHhhHhHhHhH
		HhHhHhHhHhHhHhHh
		HhHhHhHhHhHhHhHh
		hHhHhHhHhHhHhHhH
		hHhHhHhHhHhHhHhH
		hHhHhHhHHhssssHh
	]]
	local part2 = [[
		hHhHhHhHHseSSesh
		HhHhHhHhhSESSESH
	]]
	if math.random() > 0.5 then
		part2 = [[
			hHhHhHhHHsSSSSsh
			HhHhHhHhhEeSSeEH
		]]
	end
	local part3 = [[
		HhHhHhHhhSSSSSSH
		HhHhHhHhHSSllSSh
		HhHhHhHhHSSSSSSh
	]]
	return k_palette.embedded(palette, part1 .. part2 .. part3)
end

local function load_skin(obj)
	local meta = obj:get_meta()
	local skin = meta:get_string("k_player_model_skin")
	local skin_upgraded = false
	-- upgrades {
	-- } skin if present must be in the current format by now
	if #skin ~= 256 then
		-- skin is invalid or not present
		skin = gen_default()
		skin_upgraded = true
	end
	if skin_upgraded then
		meta:set_string("k_player_model_skin", skin)
	end
	return skin
end

local function update_player_model(obj)
	local skin = load_skin(obj)
	local properties = {
		visual = "mesh",
		mesh = "k_player_model.x",
		visual_size = {x = 1, y = 1, z = 1},
		-- COMMENT FOR DEBUGGING ONLY, THIS WILL BREAK THINGS
		textures = {k_palette.texture(skin, 16, 16)}
	}
	local meta = obj:get_meta()
	if meta:get_int("k_player_model_skin_preview_mode") ~= 0 then
		-- this doesn't work? >.>
		properties.glow = -1
	else
		properties.glow = 0
	end
	obj:set_properties(properties)
end

local players_to_update = {}
minetest.register_on_joinplayer(function (obj)
	update_player_model(obj)
	players_to_update[obj] = true
end)
minetest.register_on_leaveplayer(function (obj)
	players_to_update[obj] = nil
end)

local xt = 0
minetest.register_globalstep(function (time)
	xt = xt + time
	local y_offset = math.sin(xt * 1.5) * 0.5

	for obj, _ in pairs(players_to_update) do
		-- angle magnitude control --
		local angle_magnitude = 5.625
		local angle_speed = 1.0
		if obj:get_meta():get_int("k_player_model_skin_preview_mode") ~= 0 then
			angle_magnitude = 45
			angle_speed = 0.5
		end
		-- angle calculations ; basically ux magic --
		local lr_angle = math.sin(xt * angle_speed) * angle_magnitude
		local ud_angle = math.cos(xt * angle_speed) * angle_magnitude
		local roll = math.sin(xt * 1.12) * 3
		-- this bit is very important --
		ud_angle = ud_angle + ((obj:get_look_vertical() / math.pi) * 180)

		obj:set_bone_position("Armature_head", {x = 0, y = 15 + y_offset, z = 0}, {x = 90 - ud_angle, y = lr_angle, z = 180 + roll})
	end
end)

-- The Skin Editor --

local function send_skin_editor(obj)
	local skin = load_skin(obj)
	local meta = obj:get_meta()
	local c_r = meta:get_int("k_player_model_skin_r")
	local c_g = meta:get_int("k_player_model_skin_g")
	local c_b = meta:get_int("k_player_model_skin_b")
	local tool = meta:get_int("k_player_model_skin_tool")
	local preview_mode = meta:get_int("k_player_model_skin_preview_mode")

	local gen = ""
	local idx = 1
	for y = 0, 15 do
		for x = 0, 15 do
			local xp = 3 + (x / 3.5)
			local yp = y / 3
			gen = gen .. "image_button[" .. xp .. "," .. yp .. ";0.5,0.5;" .. k_palette.pixel(skin:byte(idx)) .. ";p" .. idx .. ";]"
			idx = idx + 1
		end
	end
	for v = 0, k_palette.MAX_COMPONENT do
		local y = 1 + ((k_palette.MAX_COMPONENT - v) / 2)
		local lr, lg, lb = "", "", ""
		if v == c_r then lr = "---" end
		if v == c_g then lg = "---" end
		if v == c_b then lb = "---" end
		gen = gen .. "image_button[0," .. y .. ";1,0.5;" .. k_palette.pixel(k_palette.combine(v, c_g, c_b)) .. ";cr" .. v .. ";" .. lr .. "]"
		gen = gen .. "image_button[1," .. y .. ";1,0.5;" .. k_palette.pixel(k_palette.combine(c_r, v, c_b)) .. ";cg" .. v .. ";" .. lg .. "]"
		gen = gen .. "image_button[2," .. y .. ";1,0.5;" .. k_palette.pixel(k_palette.combine(c_r, c_g, v)) .. ";cb" .. v .. ";" .. lb .. "]"
	end
	if tool == 0 then
		gen = gen .. "button[0,0;3,1;tool;Eyedropper OFF]"
	elseif tool == 1 then
		gen = gen .. "button[0,0;3,1;tool;Eyedropper ON]"
	end
	if preview_mode == 0 then
		gen = gen .. "button[0,4.5;3,1;prev;Preview Mode OFF]"
	else
		gen = gen .. "button[0,4.5;3,1;prev;Preview Mode ON]"
	end
	minetest.show_formspec(obj:get_player_name(), "k_player_model:skin_editor", [[
		size[8,5.333]
		position[0.2,0.5]
		label[0,4;R]
		label[1,4;G]
		label[2,4;B]
	]] .. gen)
end

minetest.register_on_player_receive_fields(function (obj, form, fields)
	if form == "k_player_model:skin_editor" then
		local skin = load_skin(obj)
		local meta = obj:get_meta()
		local col = k_palette.combine(
			meta:get_int("k_player_model_skin_r"),
			meta:get_int("k_player_model_skin_g"),
			meta:get_int("k_player_model_skin_b")
		)
		local tool = meta:get_int("k_player_model_skin_tool")

		local stay = false

		if fields["tool"] then
			stay = true
			if tool == 0 then
				tool = 1
			else
				tool = 0
			end
		end

		for v = 0, k_palette.MAX_COMPONENT do
			local function u(c)
				if fields["c" .. c .. v] then
					stay = true
					meta:set_int("k_player_model_skin_" .. c, v)
				end
			end
			u("r") u("g") u("b")
		end

		for idx = 1, 256 do
			if fields["p" .. idx] then
				-- Ok, what to do about this?
				if tool == 0 then
					-- Set it.
					skin = skin:sub(1, idx - 1) .. string.char(col) .. skin:sub(idx + 1)
					meta:set_string("k_player_model_skin", skin)
					update_player_model(obj)
				else
					local c_r, c_g, c_b = k_palette.split(skin:byte(idx))
					meta:set_int("k_player_model_skin_r", c_r)
					meta:set_int("k_player_model_skin_g", c_g)
					meta:set_int("k_player_model_skin_b", c_b)
					tool = 0
				end
				stay = true
			end
		end

		if fields["prev"] then
			local op = 1
			if meta:get_int("k_player_model_skin_preview_mode") ~= 0 then
				op = 0
			end
			meta:set_int("k_player_model_skin_preview_mode", op)
			update_player_model(obj)
			stay = true
		end

		if stay then
			meta:set_int("k_player_model_skin_tool", tool)
			send_skin_editor(obj)
		end
		return true
	end
end)

table.insert(k_menu, {"Edit Head...", send_skin_editor})

