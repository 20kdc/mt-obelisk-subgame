local function recompile_player_formspec(obj, mod_factoid)
	local meta = obj:get_meta()
	local factoid_idx = (meta:get_int("current_factoid") or 1) + mod_factoid
	if meta:get_int("factoid_version") ~= k_factoids.VERSION then
		-- always reset to the system changelog
		factoid_idx = 1
	end
	if factoid_idx < 1 then factoid_idx = 1 end
	if factoid_idx > #k_factoids then factoid_idx = #k_factoids end
	meta:set_int("current_factoid", factoid_idx)
	meta:set_int("factoid_version", k_factoids.VERSION)
	local factoid = "Factoid " .. factoid_idx .. "/" .. #k_factoids .. "\n" .. k_factoids[factoid_idx]
	obj:set_inventory_formspec([[
		size[11.5,8]
		label[0,0;]] .. minetest.formspec_escape(factoid) .. [[]
		button[9.5,0;1,1;factoid_dec;<]
		button[10.5,0;1,1;factoid_inc;>]
		button[9.5,1;2,1;extensions;Menu...]
		label[0,3.5;inventory]
		label[8.5,3.5;crafting]
		list[current_player;main;0,4;8,4;]
		list[current_player;craft;8.5,4;3,3;]
		list[current_player;craftpreview;9.5,7;1,1;]
	]])
end

minetest.register_on_player_receive_fields(function (obj, form, fields)
	if form == "" then
		if fields.factoid_dec then
			recompile_player_formspec(obj, -1)
		end
		if fields.factoid_inc then
			recompile_player_formspec(obj, 1)
		end
		if fields.extensions then
			k_menu.send(obj:get_player_name())
		end
		return true
	end
end)

minetest.register_on_newplayer(function (obj)
	local meta = obj:get_meta()
	meta:set_int("factoid_version", k_factoids.VERSION)
end)

minetest.register_on_joinplayer(function (obj)
	recompile_player_formspec(obj, 0)
end)

