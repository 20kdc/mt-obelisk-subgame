-- just dump them in here with table.insert
k_factoids = {}

table.insert(k_factoids, [[
RECENT CHANGES TO OBELISK:
Fixed worldgen crash issue
]])

--[[
made the compressed textures better
internal cleanup of player module
introduced new playermodel w/ an okayish texture
21.feb.2020.THE PLAYER CUSTOMIZATION UPDATE!!!
--]]

table.insert(k_factoids, [[
PLAYERS AND RULES:
This world's only rule is that if the server administrator is FORCED to intervene, they will.
Your creations are not safe.
And if someone is cheating, your only recourse is to cheat harder.
While this is obvious to any experienced internet user, it should be restated:
DO NOT SHARE PERSONAL INFORMATION.
The server administrator can't be held responsible for ignorance.
]])

table.insert(k_factoids, [[
ON PLAYER CUSTOMIZATION
In the Menu (accessible to the right), there's a button.
"Edit Head..." allows you to edit your visual appearance.
Every single pixel on the 16x16 texture is editable and visible to others.
Have fun with this!
I'm pretty sure this is a unique feature; other servers require admin intervention.
]])

k_factoids.VERSION = 4

