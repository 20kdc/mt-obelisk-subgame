k_nodes_natural = {}

table.insert(k_factoids, [[
THE NATURAL NODES
There are four natural nodes here.
Dust, substrate, lumastrate, and orb.
Dust and substrate are basic structural elements.
Lumastrate provides much of the natural light you may be seeing.
Orb is a very rare element, used for more complex; and expensive; things.
These are the elements from which things are derived.
]])

minetest.register_node("k_nodes_natural:light", {
	description = "dust",
	is_ground_content = false,
	tiles = {"k_nodes_natural_light.png"},
	groups = {k = 1}
})

minetest.register_node("k_nodes_natural:light_alt", {
	description = "dust (STUPID EDITION)",
	is_ground_content = false,
	tiles = {"k_nodes_natural_light_alt.png"},
	groups = {k = 1},
	drop = "k_nodes_natural:light"
})

minetest.register_node("k_nodes_natural:heavy", {
	description = "substrate",
	is_ground_content = false,
	tiles = {"k_nodes_natural_heavy.png"},
	groups = {k = 2},
})

minetest.register_node("k_nodes_natural:heavy_alt", {
	description = "substrate (STUPID EDITION)",
	is_ground_content = false,
	tiles = {"k_nodes_natural_heavy_alt.png"},
	groups = {k = 2},
	drop = "k_nodes_natural:heavy"
})

minetest.register_node("k_nodes_natural:glow", {
	description = "lumastrate",
	is_ground_content = false,
	tiles = {"k_nodes_natural_glow.png"},
	groups = {k = 1},
	paramtype = "light",
	sunlight_propagates = true,
	light_source = minetest.LIGHT_MAX
})

table.insert(k_factoids, [[
ORBS IN FURTHER DETAIL
Orbs are unplacable. Orbs have some special properties.
They emit a sound, and have a visible shifting 'aura' around them.
They are powerful objects, and rare in comparison to the other nodes.
But when nearby, they are easy to locate by ear... or by cheating.
This informational booklet does not support images.
However, I think you'll know an Orb when you see one.
]])

minetest.register_node("k_nodes_natural:orb", {
	drawtype = "mesh",
	mesh = "k_nodes_natural_orb.obj",
	description = "orb",
	is_ground_content = false,
	tiles = {"k_nodes_natural_orb.png"},
	groups = {k = 2, k_cannot_swap = 1},
	paramtype = "light",
	sunlight_propagates = true,
	light_source = minetest.LIGHT_MAX
	-- Orbs didn't end up that useful, so this is being left out. The voice lines are spooks .4 & .5.
--[[
	on_place = function (itemstack, placer, pointed_thing)
		minetest.sound_play("k_nodes_natural_noplaceorb", {
			pos = vector.add(placer:get_pos(), vector.new((math.random() - 0.5) * 5, (math.random() - 0.5) * 5, (math.random() - 0.5) * 5)),
			to_player = placer:get_player_name(),
			gain = 0.125
		}, true)
		return itemstack
	end
--]]
})

minetest.register_entity("k_nodes_natural:orbfx", {
	_orbtimer = 0,
	initial_properties = {
		physical = false,
		static_save = false,
		pointable = false,
		visual = "mesh",
		use_texture_alpha = true,
		mesh = "k_nodes_natural_orb.obj",
		textures = {"k_nodes_natural_orb.png^k_nodes_natural_orb_triangle.png"},
		automatic_rotate = 0.5,
		visual_size = {x = 11, y = 11, z = 11},
		backface_culling = false
	},
	on_step = function (self, dtime)
		self._orbtimer = self._orbtimer + dtime
		if self._orbtimer < 3.2 then
			return
		end
		self._orbtimer = 0
		local node_pos = self.object:get_pos()
		node_pos.x = math.floor(node_pos.x + 0.5)
		node_pos.y = math.floor(node_pos.y + 0.5)
		node_pos.z = math.floor(node_pos.z + 0.5)
		if minetest.get_node(node_pos).name ~= "k_nodes_natural:orb" then
			self.object:remove()
			return
		end
		local pitches = {
			0.5,
			0.75,
			1,
			1.25,
			1.5
		}
		minetest.sound_play("k_nodes_natural_orbfx", {
			object = self.object,
			pitch = pitches[math.random(#pitches)]
		}, true)
	end
})

minetest.register_abm({
	label = "give sounds to orbs",
	nodenames = {"k_nodes_natural:orb"},
	interval = 1,
	chance = 1,
	catch_up = true,
	action = function (pos)
		-- we don't really care about anything except pos
		if #minetest.get_objects_inside_radius(pos, 0.5) == 0 then
			minetest.add_entity(pos, "k_nodes_natural:orbfx")
		end
	end
})
