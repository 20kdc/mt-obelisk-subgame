minetest.register_chatcommand("system_alert", {
	params = "some text",
	description = "used by administrators",
	privs = { shout = true },
	func = function (name, param)
		minetest.chat_send_all("GLOBAL SYSTEM ALERT: " .. param)
	end
})

