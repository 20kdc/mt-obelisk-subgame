minetest.register_on_generated(function (minp, maxp, seed)
	local vm, emin, emax = minetest.get_mapgen_object("voxelmanip")
	local area = VoxelArea:new({
		MinEdge = emin,
		MaxEdge = emax
	})

	local noise = minetest.get_perlin_map({
		offset = minp.y / 256,
		scale = 1,
		spread = vector.new(8, 8, 8),
		seed = 1231,
		octaves = 2,
		persist = 0
	}, vector.add(vector.subtract(emax, emin), vector.new(1, 1, 1))):get3dMap_flat(emin)

	local data = vm:get_data()

	local cid1 = minetest.get_content_id("k_nodes_natural:light")
	local cid1b = minetest.get_content_id("k_nodes_natural:light_alt")
	local cid2 = minetest.get_content_id("k_nodes_natural:heavy")
	local cid2b = minetest.get_content_id("k_nodes_natural:heavy_alt")
	local cid3 = minetest.get_content_id("k_nodes_natural:glow")
	local cid4 = minetest.get_content_id("k_nodes_natural:orb")
	for i in area:iterp(emin, emax) do
		local use_alt = (i + math.floor(i / 256)) % 2 == 0
		-- really light: glow
		if noise[i] < -0.8 then
			data[i] = cid3
		end
		-- as it gets heavier use light & then heavy
		if noise[i] > 0.25 then
			data[i] = cid1
			if use_alt then
				data[i] = cid1b
			end
		end
		if noise[i] > 0.9 then
			data[i] = cid2
			-- if it's ALREADY heavy, then give it a tiny chance of being Orb
			-- this is calibrated so that even in the substrate sea, they're few & far between
			-- (why a million? well 1 in 100,000 seems to not be that rare in practice)
			-- EDIT FROM THE FUTURE: Ok so that was TOO rare.
			-- I'm going to set it to 1 in 10,000. That'll be at least rareish, but won't be broken.
			if math.random() > 0.9999 then
				data[i] = cid4
			elseif use_alt then
				data[i] = cid2b
			end
		end
	end
	vm:set_data(data)
	vm:set_lighting{day = 0, night = 0}
	vm:calc_lighting()
	vm:write_to_map()
end)

minetest.after(0, function ()
	minetest.set_timeofday(0)
end)

table.insert(k_factoids, [[
THE STRUCTURE OF THE NATURAL WORLD
To create any structures, you must have nodes. Luckily, these are plentiful.
The world you are in is essentially in 4 layers, blended together.
Lumastrate, air, dust, and substrate.
As you go further down, you go towards an endless lumastrate sea.
As you go further up, you go towards an endless substrate sea.
Dust occupies a thick boundary between air and substrate.
]])

