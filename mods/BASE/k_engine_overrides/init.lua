minetest.register_tool(":", {
	description = "empty",
	is_ground_content = false,
	inventory_image = "wieldhand.png",
	groups = {k = 1},
	tool_capabilities = {
		max_drop_level = 2,
		groupcaps = {
			k = {times = {0.25, 0.5}, maxlevel = 2}
		}
	}
})

minetest.register_on_joinplayer(function (obj)
	obj:set_sky("#000000", "plain", {}, false)
	obj:set_physics_override({
		sneak_glitch = true,
		new_move = false
	})
end)

