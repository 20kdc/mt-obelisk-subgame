-- don't worry, for 16x16 at least this works fine
-- theoretically it should go up to at least 64x32 without issue
k_palette = {}
k_palette.MAX_COMPONENT = 5

local mul = k_palette.MAX_COMPONENT + 1

function k_palette.combine(r, g, b)
	return b + ((g + (r * mul)) * mul)
end
function k_palette.split(col)
	return
		math.floor(col / (mul * mul)) % mul,
		math.floor(col / mul) % mul,
		col % mul
end

function k_palette.pixel(col)
	return string.format("k%02x.png", col)
end

function k_palette.texture(raw, w, h)
	local components = {
		"[combine",
		w .. "x" .. h
	}
	local i = 1
	for y = 0, h - 1 do
		for x = 0, w - 1 do
			table.insert(components, x .. "," .. y .. "=" .. k_palette.pixel(raw:byte(i)))
			i = i + 1
		end
	end
	return table.concat(components, ":")
end

-- Exchanges colours between the BytePusher (standard) and Web (GIMP) indexes
function k_palette.webex(i)
	return 215 - i
end

-- Note: If using this, PNG mirrors of the content need to be available
-- Use the "Web" palette
function k_palette.embedded(palette, text)
	local total = ""
	for i = 1, #text do
		local c = palette[text:sub(i, i)]
		if c then
			total = total .. string.char(k_palette.webex(c))
		end
	end
	return total
end

