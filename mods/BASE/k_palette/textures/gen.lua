local colours = {}
local channel = {
	[0] = "00",
	"33",
	"66",
	"99",
	"cc",
	"ff"
}
for r = 0, 5 do
	for g = 0, 5 do
		for b = 0, 5 do
			table.insert(colours, "#" .. channel[r] .. channel[g] .. channel[b])
		end
	end
end

while #colours < 256 do table.insert(colours, "#000000") end

for k, v in ipairs(colours) do
	os.execute("convert -size 1x1 canvas:" .. v .. " k" .. string.format("%02x", k - 1) .. ".png")
end

