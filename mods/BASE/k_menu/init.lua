-- Insert functions as {"NAME", function (obj)}
k_menu = {}
k_menu.send = function (name)
	local fullmenu = {"size[4," .. #k_menu .. "]"}
	for k, v in ipairs(k_menu) do
		table.insert(fullmenu, "button_exit[0," .. (k - 1) .. ";4,1;b" .. k .. ";" .. minetest.formspec_escape(v[1]) .. "]")
	end
	minetest.show_formspec(name, "k_menu:menu", table.concat(fullmenu))
end
minetest.register_on_player_receive_fields(function (obj, form, fields)
	if form == "k_menu:menu" then
		for k, v in ipairs(k_menu) do
			if fields["b" .. k] then
				v[2](obj)
			end
		end
		return true
	end
end)

minetest.register_chatcommand("menu", {
	description = "show the subgame menu",
	func = function (name)
		if name then
			k_menu.send(name)
		end
	end
})

