local music = dofile(minetest.get_modpath("k_player_music") .. "/music.lua")

local music_vm_stop = {}
minetest.register_on_joinplayer(function (obj)
	music_vm_stop[obj] = music(obj)
end)
minetest.register_on_leaveplayer(function (obj)
	music_vm_stop[obj]()
	music_vm_stop[obj] = nil
end)

