-- MUSIC MACHINE --
return function (obj)
	local stop = false
	-- MUSICAL LIBRARY --
	local function at(time, fn)
		minetest.after(time, function ()
			if not stop then fn() end
		end)
	end
	local function c(fn, ...)
		local args = {...}
		return function ()
			return fn(unpack(args))
		end
	end
	local function px(name, pitch, gain)
		if not stop then
			minetest.sound_play(name, {
				to_player = obj:get_player_name(),
				pitch = pitch, -- if nil, doesn't count
				gain = gain
			}, true)
		end
	end
	local function p(name, len)
		return function (ret)
			px(name)
			at(len, ret)
		end
	end
	local function rpitch() return 1 + (math.random() - 0.5) end
	-- MUSICAL INTENSITY CONTROLLER --
	local c_sequence, c_intensity = "null", 0
	local next_pattern
	local sequences = {
		null = function () c_intensity = -1 at(4, next_pattern) end
	}
	local decide_sequence
	next_pattern = function ()
		local wanted = decide_sequence()
		if wanted ~= c_sequence and c_intensity <= -1 then
			c_sequence = wanted
			c_intensity = 0
		end
		local intensity = c_intensity
		if wanted == c_sequence then
			c_intensity = c_intensity + 1
		else
			c_intensity = c_intensity - 1
		end
		sequences[c_sequence](intensity, wanted == c_sequence)
	end
	-- MAIN MUSICAL ELEMENTS --
	local RO_LEN = 1.8
	local ro1, ro2, ro3 = p("k_player_ro1", RO_LEN), p("k_player_ro2", RO_LEN), p("k_player_ro3", RO_LEN)
	-- MAIN MUSICAL SEQUENCES --
	function sequences.deep(i, up)
		if i == 0 or i == 1 then
			ro1(c(at, RO_LEN * 3, next_pattern))
		elseif i == 2 or i == 3 then -- PART 1
			ro1(c(ro2, c(ro3, c(at, RO_LEN, next_pattern))))
			if not up then
				c_intensity = 1
			end
		elseif i == 4 then
			ro3(c(ro2, c(ro1, c(at, RO_LEN, next_pattern))))
			if not up then
				c_intensity = 1
			end
		elseif i == 5 then
			ro3(c(ro2, c(ro3, c(at, RO_LEN, next_pattern))))
			if not up then
				c_intensity = 1
			end
		elseif i == 6 or i == 7 then -- PART 2
			ro1(c(ro2, c(ro3, c(at, RO_LEN, next_pattern))))
			px("k_player_sp4")
			if not up then
				c_intensity = 2
			end
		elseif i == 8 then
			ro3(c(ro2, c(ro1, c(at, RO_LEN, next_pattern))))
			px("k_player_sp5")
			if not up then
				c_intensity = 4
			end
		elseif i == 9 then
			ro3(c(ro2, c(ro3, c(at, RO_LEN, next_pattern))))
			px("k_player_sp6")
			if up then
				c_intensity = 2
			else
				c_intensity = 5
			end
		end
	end
	function sequences.high(i, up)
		if not up then
			-- prevent the hum sticking around for TOO long
			-- the intensity values in this sequence are primarily used to delay spooks
			-- i.e. you don't receive spooks unless you've been in there for at least ~45 seconds
			if c_intensity > 1 then
				c_intensity = 1
			end
		end
		-- only leave true for debugging spooks
		local dospook = false
		if c_intensity > 9 then
			c_intensity = 9
			dospook = dospook or (math.random() > 0.5)
		end
		if dospook then
			-- this is pretty specific code
			minetest.sound_play("k_player_spook", {
				pos = vector.add(obj:get_pos(), vector.new((math.random() - 0.5) * 10, (math.random() - 0.5) * 10, (math.random() - 0.5) * 10)),
				to_player = obj:get_player_name(),
				pitch = rpitch(),
				gain = 0.125
			}, true)
			c_intensity = 7
		end
		px("k_player_hum", rpitch())
		at(5, next_pattern)
	end
	-- END MAIN MUSICAL CODE, BEGIN SEQUENCE DECIDER --
	decide_sequence = function ()
		local pos = obj:get_pos()
		if pos.y < -55 then
			return "deep"
		end
		if pos.y > 55 then
			return "high"
		end
		return "null"
	end
	-- END SEQUENCE DECIDER --
	next_pattern()
	return function ()
		stop = true
	end
end
